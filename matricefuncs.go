package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sfmathe/sfrechner/maths"
)

func determinant(myApp fyne.App, mat maths.Matrix) {
	var str string
	val, err := mat.Determinant()
	str = fmt.Sprintf("Die Determinante beträgt %f", val)
	if err != nil{
		str = fmt.Sprintf("Fehler: %s", err)
	}
	showwindow(widget.NewLabel(str), myApp, "Determinante")
}

func ref(myApp fyne.App, mat maths.Matrix) {
	var cont fyne.CanvasObject
	arr, err := mat.Ref()
	if err != nil{
		cont = widget.NewLabel(fmt.Sprintf("Fehler: %s", err))
	}

	cont = displaymatrix(arr)
	showwindow(cont, myApp, "ref")
}

func rref(myApp fyne.App, mat maths.Matrix) {	 
	var cont fyne.CanvasObject
	arr, err := mat.Rref()
	if err != nil{
		cont = widget.NewLabel(fmt.Sprintf("Fehler: %s", err))
	}

	cont = displaymatrix(arr)
	showwindow(cont, myApp, "ref")
}