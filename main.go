package main

import (
	"fmt"

	"gitlab.com/sfmathe/sfrechner/maths"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {
	myApp := app.New()

	myWindow := myApp.NewWindow("SFRechner")
	myWindow.SetFixedSize(false)
	myWindow.Resize(fyne.NewSize(1280, 720))

	myWindow.SetMaster()

	maths.NewMatrix([][]float64{{0.0}, {0.0}})
	tabs := container.NewAppTabs(
		container.NewTabItem("Physik", genphystab(myApp)),
		container.NewTabItem("Mathe", genmathtab(myApp)),
	)
	myWindow.SetContent(tabs)
	myWindow.ShowAndRun()
}

// creating all the Physics related Tabs

func genphystab(myApp fyne.App) fyne.CanvasObject {
	subtitle := fmt.Sprintf(` Hier lassen sich alle Funktion bezüglich dem Physikteil des SF PAM auffinden. `)
	fotabe := fmt.Sprintf(`Im FoTaBe befindet sich der Phsyik-Teil ab Seite 155.`)
	header := widget.NewCard("Physik", subtitle, widget.NewLabel(fotabe))
	tabs := container.NewAppTabs(
		container.NewTabItem("Konstanten", Genconsttab()),
		container.NewTabItem("Optik", genoptictab(myApp)),
		container.NewTabItem("Wellenlehre", genwavetab(myApp)),
		container.NewTabItem("Radioaktivität", genradioactivitytab(myApp)),
		container.NewTabItem("Relativitätstheorie", genrelativitytab(myApp)),
	)
	cont := container.NewVBox(header, tabs)
	return cont
}

func genmathtab(myApp fyne.App) fyne.CanvasObject {
	subtitle := fmt.Sprintf(` Hier lassen sich alle Funktion bezüglich dem Matheteil des SF PAM auffinden. `)
	fotabe := fmt.Sprintf(`Im FoTaBe befindet sich der Phsyik-Teil von Seite 1 bis 154.`)
	header := Makeheader("Mathe", subtitle, fotabe)
	tabs := container.NewAppTabs(
		container.NewTabItem("Beurteilende Statistik", genstatstab(myApp)),
		container.NewTabItem("Matrizen", genmatrixtab(myApp)),
		container.NewTabItem("RSA-Verschlüsselung", genrsatab(myApp)),
	)
	cont := container.NewVBox(header, tabs)
	return cont
}



