package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sfmathe/sfrechner/physics"
)

func eval(decay physics.Zerfallsfunktion, myApp fyne.App, t float64) {
	val := decay.Eval(t)

	str := fmt.Sprintf("Der Wert nach %f Zeiteinheiten beträgt %e", t, val)

	showwindow(widget.NewLabel(str), myApp, "Berechnung")
}

func activity(decay physics.Zerfallsfunktion, myApp fyne.App, N float64) {
	val := decay.Activity(N)

	str := fmt.Sprintf("Die Aktivität bei einer Anzahl Kerne von %f beträgt %f Becquerel", N, val)

	showwindow(widget.NewLabel(str), myApp, "Aktivität")
}

func midlife(decay physics.Zerfallsfunktion, myApp fyne.App){
	val := decay.Avglife()

	str := fmt.Sprintf("Die mittlere Lebensdauer eines Kernes entspricht %f s", val)
	
	showwindow(widget.NewLabel(str), myApp, "Mittlere Lebensdauer")
}
