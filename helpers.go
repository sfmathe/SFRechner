package main

import (
	"fmt"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func Maptoarr(m map[string]float64) ([]string, []float64) {
	var values []float64
	var keys []string
	for key, value := range m {
		keys = append(keys, key)
		values = append(values, value)
	}

	return keys, values
}

func Makeheader(title, subtitle, fotabe string) fyne.CanvasObject {
	header := widget.NewCard(title, subtitle, widget.NewLabel(fotabe))
	return header
}

func floatlabelf(label string, vals ...float64) *widget.Label {
	return widget.NewLabel(fmt.Sprintf(label, vals))
}


func Makeentries(parameters ...string) map[string]*widget.Entry {
	var entries = make(map[string]*widget.Entry)
	for _, i := range parameters {
		entry := widget.NewEntry()
		entries[i] = entry
	}

	return entries
}


func Makefloats(entries map[string]*widget.Entry) (map[string]float64) {
	var floats = make(map[string]float64)
	for key, val := range entries {
		str := val.Text
		if i, err := strconv.ParseFloat(str, 64); err == nil {
			floats[key] = i
		}
	}
	return floats
}

func Appendform(form *widget.Form, entries map[string]*widget.Entry, paras map[string]string) {
	// make keys 
	var keys []string
	for key := range entries{
		keys = append(keys, key)
	}
	
	
	// append for each key
	for _, key := range keys{
		form.Append(fmt.Sprintf("%s %s in SI-Einheiten", paras[key], key), entries[key])
	}
}

func strconvnoerr(str string) float64 {
	if float, err := strconv.ParseFloat(str, 64); err == nil{
		return float
	}
	return 0
}

func showwindow(obj fyne.CanvasObject, myApp fyne.App, title string) {
	w := myApp.NewWindow(title)
	w.SetContent(obj)
	w.Show()
}

func uimatrix(rows int, columns int) (fyne.CanvasObject, map[string]*widget.Entry ){
	var cont = container.New(layout.NewVBoxLayout())
	
	var entries = map[string]*widget.Entry{}
	var str string

	for i := 0; i < rows ; i++ {
		var hcont = container.New(layout.NewHBoxLayout())
		for j := 0; j < columns; j++{
			str = fmt.Sprintf("%d,%d", i, j)
			entries[str] = widget.NewEntry()
			hcont.Objects = append(hcont.Objects, entries[str])
		}
		cont.Objects = append(cont.Objects, hcont)
	}
	
	return cont, entries
}

func uimatrixtoarr(entries map[string]*widget.Entry, rows int, cols int) [][]float64 {
	array := [][]float64{}
	for i := 0; i < rows; i++{
		row := []float64{}
		for j := 0; j < cols; j++{
			str := fmt.Sprintf("%d,%d", i, j)
			float := strconvnoerr(entries[str].Text)
			row = append(row, float)
		}
		array = append(array, row)
	}
	return array
}

func displaymatrix(arr [][]float64) fyne.CanvasObject {
	var matrice = container.New(layout.NewVBoxLayout())
	var solution = container.New(layout.NewVBoxLayout())
	var str string

	for row := range arr{
		str = fmt.Sprintf("Der %d. Parameter trägt den Wert %f", row + 1, arr[row][len(arr[0])-1])
		label := widget.NewLabel(str)
		solution.Objects = append(solution.Objects, label)
		matrow := container.New(layout.NewHBoxLayout())
		for col := range arr[row]{
				
			str := fmt.Sprintf("%f", arr[row][col])
			matrow.Objects = append(matrow.Objects, widget.NewLabel(str))
		}
		matrice.Objects = append(matrice.Objects, matrow)
	}

	cont := container.New(layout.NewVBoxLayout(), matrice, solution)

	return cont
}