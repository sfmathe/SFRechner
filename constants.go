package main

import "fmt"


var lambda = fmt.Sprintf("%q", '\u03BB')
var phi = fmt.Sprintf("%q", '\u03C6')

var opticparas = map[string]string{
	"f": "Die Brennweite",
	"D": "Die Brechkraft",
	"b": "Die Bildweite",
	"B": "Die Bildgrösse",
	"g": "Die Gegenstandsweite",
	"G": "Die Gegenstandsgrösse",
}

var waveparas = map[string]string{
	lambda : "Die Wellenlänge",
	"T" : "Die Periode",
	"f" : "Die Frequenz",
	"v" : "Die Geschwindigkeit",
	"w" : "Die Kreisfrequenz",
	"k" : "Die Kreiswellenzahl",
}

var slitparas = map[string]string{
	"d" : "Die Gitterkonstante bzw. der Spaltabstand",
	"D" : "Die Distanz vom Spalt zum Detektor",
	lambda : "Die Wellenlänge",
	"n" : "Die Ordnung der der Extremstelle",
}
var slitmap = map[string]int{
	"Einzelspalt" : 1,
	"Gitter" : 2,
}
