package maths

import "math"

func simpleeuclid(a int64, b int64) int64 {
	if b == 0 {
		return a
	}
	if a < b { // if a is smaller than b switch numbers
		temp := a
		a, b = b, temp
	}
	next := a % b

	return simpleeuclid(b, next)
}

func Exteuclid(e int, n int, x *int, y *int) int {
	if n == 0 {
		*x = 1
		*y = 0
		return e
	}
	var x1, y1 int
	gcd := Exteuclid(n, e%n, &x1, &y1)
	*x = y1
	*y = x1 - y1*(int(math.Floor(float64(e)/float64(n))))

	return gcd
}

