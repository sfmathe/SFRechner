package maths

import (
	"errors"
)

var wrongsizeerr error = errors.New(
	"This program only supports finding determinants of 3x3 or 2x2 matrices. \n For other matrices consult BeFoTa Page 31",
)
var nopivotfound error = errors.New(
	"No pivot was found in Matrix",
)

type Matrix struct {
	array   [][]float64
	rows    int
	columns int
}

func NewMatrix(array [][]float64) *Matrix {
	m := new(Matrix)
	m.array = array
	m.rows = len(array)
	m.columns = len(array[0])
	return m
}
func (m Matrix) Checksize() error {
	if m.rows != m.columns {
		return wrongsizeerr
	}
	if m.rows == 2 || m.rows == 3 {
		return nil
	}
	return wrongsizeerr

}

func (m Matrix) Determinant() (float64, error) {
	if err := m.Checksize(); err != nil {
		return 0, wrongsizeerr
	}

	switch {
	case m.rows == 3:
		a := m.array[0][0] * m.array[1][1] * m.array[2][2]
		b := m.array[0][1] * m.array[1][2] * m.array[2][0]
		c := m.array[0][2] * m.array[1][0] * m.array[2][1]
		val := a + b + c
		d := m.array[0][2] * m.array[1][1] * m.array[2][0]
		e := m.array[0][0] * m.array[1][2] * m.array[2][1]
		f := m.array[0][1] * m.array[1][0] * m.array[2][2]
		subval := d + e + f
		return val - subval, nil

	case m.rows == 2:
		val := m.array[0][0] * m.array[1][1]
		subval := m.array[0][1] * m.array[1][0]
		return val - subval, nil

	}
	return 0, wrongsizeerr
}
func (m Matrix) Getcolumn(nr int) []float64 {
	var column []float64
	for i := 0; i < m.rows; i++ {
		column = append(column, m.array[i][nr-1])
	}
	return column

}

// bring Matrix into row echelon form
func (m Matrix) Ref() ([][]float64, error) {
	var ignoredrows []int
	var refMatrix [][]float64
	ignoredrows = []int{}
	refMatrix = m.array
	// current row, right now that is he the first row, will be incremented
	for rowcurr, colcurr := 0, 0; 
	rowcurr < len(refMatrix) && colcurr < len(refMatrix[0]);
	 rowcurr, colcurr = rowcurr+1, colcurr+1 {
		// find "pivot" of first column, e.g. first non-zero entry
		pivot, err := m.Findpivot(rowcurr+1, ignoredrows) //
		if err != nil {
			continue
		}
		row := pivot[0]

		// switch pivot row with first row
		refMatrix[rowcurr], refMatrix[row] = refMatrix[row], refMatrix[rowcurr]
		row = rowcurr

		// multiply each value in row by the inverse of pivot, making the first entry
		// equal one
		inverse := 1 / refMatrix[row][colcurr]
		for pos, i := range refMatrix[row] {
			refMatrix[row][pos] = i * inverse
		}

		// subtract multiples of pivot row to lower rows
		// such that the value of pivot column equals 0
		for i := rowcurr + 1; i < len(refMatrix) && i > rowcurr; i++ {
			multiplier := refMatrix[i][colcurr]
			for pos, j := range refMatrix[i] {
				refMatrix[i][pos] = j - (multiplier * refMatrix[rowcurr][pos])
			}
		}
		ignoredrows = append(ignoredrows, rowcurr)
	}
	return refMatrix, nil
}

// find reduced row echelon form of matrix
func (m Matrix) Rref() ([][]float64, error) {
	refMatrix, err := m.Ref()
	if err != nil {
		return [][]float64{{}, {}}, err
	}
	// working our way up, first find last row where the first non-zero entry is 1
	var pos []int
	var multiplier float64
	var Break, Continue, Bool bool

	for i := len(refMatrix) - 1; i >= 0; i-- {
		Break = false
		for j, val := range refMatrix[i] {
			Continue = false
			switch val {
			case 0:
				Continue = true
				continue

			case 1:
				Bool = true
				pos = []int{i, j}
				break

			default:
				Break = true
				break
			}
			if Bool {
				break
			}
			if Break {
				break
			}

		}
		if Break {
			break
		}
		if Continue {
			continue
		}
		// add upper rows such that all the values in the column of pivot equal 0
		for i := pos[0] - 1; i >= 0; i-- {
			multiplier = refMatrix[i][pos[1]] / refMatrix[pos[0]][pos[1]] 
			for j, val := range refMatrix[i] {
				refMatrix[i][j] = val - (multiplier * refMatrix[pos[0]][j])
			}
		}
	}

	return refMatrix, nil
}

// following functions are helper functions for the ref and rref functions

func (m Matrix) Findpivot(column int, ignore []int) ([2]int, error) {
	col := m.Getcolumn(column)
	for pos, i := range col {
		boo, _ := arrinclude(pos, ignore)
		if boo {
			continue
		}
		if i != 0 {
			return [2]int{pos, column - 1}, nil
		}
	}

	return [2]int{0, 0}, nopivotfound

}
