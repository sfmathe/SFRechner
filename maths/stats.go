package maths

import (
	"errors"
	"math"
)

//define an error to raise, when the input is not acceptible
var nosigma error = errors.New("stats: Given Security is not part of the supported Sigma-Rules")

// Binomial distribution (Verteilung)
type Binomial struct {
	P         float64 // probability for event X
	Arcp      float64 // inverse of p (Gegenwahrscheinlichkeit) = 1-p
	N        float64 // number of trials
	Deviation float64 // standard deviation (standardabweichung)
	Mean      float64 // mean of distribution e.g. expected value(Erwartungswert bzw. Durchschnitt)
}

func (obj *Binomial) Init() {
	if obj.P != 0.0 {
		obj.Arcp = 1 - obj.P
	}
	if obj.P != 0.0 && obj.N != 0.0 {
		obj.Mean = obj.P * obj.N
		obj.Deviation = math.Sqrt(obj.Mean * obj.Arcp)
	}
}

// check if given security is acceptible
func checksecurity(security int16) (float64, error) {
	var sigma float64
	switch security {
	case 99:
		sigma = 2.58
	case 95:
		sigma = 1.96
	case 90:
		sigma = 1.64
	default:
		return 0, nosigma
	}
	return sigma, nil
}

// Prediction intervall (prognoseintervall) for the most common intervalls (95%, 99%, 90%)
func (b Binomial) Prediction(security int16) ([2]float64, error) { //define how much should lie in sample, e.g. 95 percentage is
	sigma, err := checksecurity(security)
	if err != nil {
		return [2]float64{0, 0}, nosigma
	}
	c := sigma * b.Deviation
	intervall := [2]float64{b.Mean - c, b.Mean + c}
	return intervall, nil
}

// sample (stichprobe)
type Sample struct {
	H         float64 //probability from sample
	Arch      float64 // anti-probability from sample, 1-h (Gegenwahscheinlichkeit)
	N         float64 // number of samples
	Deviation float64
}

// initialise struct sample
func (obj *Sample) Init() {
	if obj.H != 0 {
		obj.Arch = 1 - obj.H
	}
	if obj.H != 0 && obj.N != 0 {
		obj.Deviation = math.Sqrt((obj.H * obj.Arch) / (obj.N))
	}
}

// calculate confidence intervall given a sample
func (s Sample) Confidence(security int16) ([2]float64, error) {
	sigma, err := checksecurity(security)
	if err != nil {
		return [2]float64{0, 0}, nosigma
	}
	x := math.Pow(sigma, 2) / s.N // constant used to solve quadratic equation 
	a := 1 + x                    // determine parameters for quadratic formula,
	B := -1*(2*s.H) - x
	c := math.Pow(s.H, 2)
	D := math.Sqrt(math.Pow(B, 2) - 4*a*c) // determine square root of discriminant
	lower := (-1*B - D) / (2 * a)          // determine lower bound of intervall
	upper := (-1*B + D) / (2 * a)         // determine upper bound of intervall
	intervall := [2]float64{lower, upper}
	return intervall, nil
}

// find out tolerance of a sample (Fehlertoleranz einer Stichprobe)
func (s Sample) Tolerance(security int16) (float64, error) {
	sigma, err := checksecurity(security)
	if err != nil {
		return 0, nosigma
	}

	return sigma * s.Deviation, nil
}

//find out sample-size

func Samplesizewithguess(guess float64, tolerance float64, security int16) (float64, error) {
	sigma, err := checksecurity(security)
	if err != nil {
		return 0, nosigma
	}
	sample := Sample{H: guess}
	sample.Init()
	num := math.Pow(sigma, 2) * sample.H * sample.Arch // define numerator of fraction (Zähler)
	den := math.Pow(tolerance, 2)                          // define denominator of fraction (nenner)
	n := num / den
	sample.N = n
	return sample.N, nil
}

//sample size without guess using the assumption that h(1-h) = 0.25
func Samplesize(tolerance float64, security int16) (float64, error) {
	sigma, err := checksecurity(security)
	if err != nil {
		return 0, nosigma
	}
	num := math.Pow(sigma, 2)         // define numerator of fraction (Zähler)
	den := 4 * math.Pow(tolerance, 2) // define denominator of fraction (nenner)
	return num / den, nil
}
