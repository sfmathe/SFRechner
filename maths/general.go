package maths

func arrinclude(item int, arr []int) (bool, int) {
	for pos, i := range arr {
		if item == i {
			return true, pos
		}
	}
	return false, -1
}
