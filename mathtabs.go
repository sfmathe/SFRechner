package main

import (
	"fmt"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sfmathe/sfrechner/maths"
)

func genstatstab(myApp fyne.App) fyne.CanvasObject {

	cont := container.NewVBox()

	subtitle := "Auf diesem Tab finden sie Berechnungen für die Beurteilende Statistik."
	fotabe := "Im FoTaBe befindet sich der Statistik-Teil von Seite 115 an."
	header := Makeheader("Beurteilende Statistik", subtitle, fotabe)

	prob := widget.NewEntry()
	probitem := widget.NewFormItem("Die Wahrscheinlichkeit", prob)
	amount := widget.NewEntry()
	amountitem := widget.NewFormItem("Die Anzahl Versuche bzw. der Stichprobenumfang", amount)
	tolerance := widget.NewEntry()
	toleranceitem := widget.NewFormItem("Die gewünschte Fehlertoleranz", tolerance)

	var distrtype, calctype, seclevel, guesstype string

	basicformarr := []*widget.FormItem{}
	statform := &widget.Form{
		Items: basicformarr,
	}

	guesschoice := widget.NewSelect([]string{"Mit Vermutung", "Ohne Vermutung"}, func(val string) {
		guesstype = val
	})
	guesschoice.Hide()

	options := []string{"99", "90", "95"}
	secchoice := widget.NewSelect(options, func(val string) {
		seclevel = val
	})

	var calcoptions = []string{"Prognoseintervall", "Konfidenzintervall",
		"Fehlertoleranz", "Stichprobenumfang"}
	calcchoice := widget.NewSelect(calcoptions, func(val string) {
		var hide bool
		calctype = val
		switch val {
		case "Prognoseintervall":
			statform.Items = append(basicformarr, probitem, amountitem)
			hide = true
		case "Konfidenzintervall":
			statform.Items = append(basicformarr, probitem, amountitem)
			hide = true
		case "Fehlertoleranz":
			statform.Items = append(basicformarr, probitem, amountitem)
			hide = true
		case "Stichprobenumfang":
			statform.Items = append(basicformarr, probitem, toleranceitem)
			guesschoice.Show()
		}
		if hide {
			guesschoice.Hide()
		}
		statform.Refresh()
		cont.Refresh()
	})

	distrchoice := widget.NewSelect([]string{"Verteilung", "Stichprobe"}, func(val string) {
		distrtype = val
		switch val {
		case "Verteilung":
			calcoptions = []string{"Prognoseintervall"}
		case "Stichprobe":
			calcoptions = []string{"Konfidenzintervall", "Fehlertoleranz", "Stichprobenumfang"}
		}
		calcchoice.Refresh()
		cont.Refresh()

	})

	statform.OnSubmit = func() {
		sec, _ := strconv.ParseInt(seclevel, 0, 16)
		switch distrtype {
		case "Verteilung":
			b := maths.Binomial{
				P: strconvnoerr(prob.Text),
				N: strconvnoerr(amount.Text),
			}
			b.Init()
			prediction(myApp, b, int16(sec))
		case "Stichprobe":
			sample := maths.Sample{
				H: strconvnoerr(prob.Text),
				N: strconvnoerr(amount.Text),
			}
			sample.Init()
			switch calctype {
			case "Konfidenzintervall":
				confidence(myApp, sample, int16(sec))
			case "Fehlertoleranz":
				errtolerance(myApp, sample, int16(sec))
			case "Stichprobenumfang":
				Samplesize(myApp, sample.H, strconvnoerr(tolerance.Text), guesstype, int16(sec))
			}
		}
	}

	cont.Objects = []fyne.CanvasObject{header, distrchoice, calcchoice, secchoice, guesschoice, statform}

	return cont
}

func genrsatab(myApp fyne.App) fyne.CanvasObject {
	subtitle := "Auf diesem Tab finden Sie ein Tool, mit dem man (kleinere) RSA Key-Pairs generieren kann."
	fotabe := "Das Eulersche Verfahren steht im FoTaBe auf Seite 36."
	header := Makeheader("RSA-Generator", subtitle, fotabe)

	//create entries for RSA-Tab

	ep := widget.NewEntry()
	itemp := widget.NewFormItem("Das gewünschte p", ep)
	eq := widget.NewEntry()
	itemq := widget.NewFormItem("Das gewünschte q", eq)
	ee := widget.NewEntry()
	iteme := widget.NewFormItem("Das gewünschte e (wenn leer wählt das Programm eine Primzahl)", ee)

	rsaform := &widget.Form{
		Items: []*widget.FormItem{itemp, itemq, iteme},
	}
	rsaform.OnSubmit = func() {
		p := int(strconvnoerr(ep.Text))
		q := int(strconvnoerr(eq.Text))
		n := p * q
		PHIn := (p-1) * (q-1)
		e := int(strconvnoerr(ee.Text))

		if e == 0 {
			if PHIn > 17{
				e = 17
			}else {
				e = 5
			}
		}
		
		var x,y int
		maths.Exteuclid(e, PHIn, &x, &y)
		if x < 0 {
			x = PHIn + x
		}
		d := x

		// make labels
		pq := widget.NewLabel(fmt.Sprintf("Bei p = %d, q = %d und e = %d :", p, q, e))
		public := widget.NewLabel(fmt.Sprintf("Der Public-Key beträgt ([e, n]) [%d, %d]", e, n))
		private := widget.NewLabel(fmt.Sprintf("Der Private-Key beträgt ([n, d]) [%d, %d]", n, d))
		philabel := widget.NewLabel(fmt.Sprintf("%s(n) beträgt %d", phi, PHIn))

		cont := container.New(layout.NewVBoxLayout(), pq, public, private, philabel)

		showwindow(cont, myApp, "RSA-Keys")
	}
	

	cont := container.New(layout.NewVBoxLayout(), header, rsaform)
	return cont
}

func genmatrixtab(myApp fyne.App) fyne.CanvasObject {

	// matrix cont
	var matrix fyne.CanvasObject
	matrix = widget.NewLabel("Bitte Zeilen und Spalten eingeben")

	// create matrix H box
	columns := widget.NewEntry()
	columnlabel := widget.NewLabel("Spalten")
	rows := widget.NewEntry()
	rowlabel := widget.NewLabel("Zeilen")
	crmatrix := container.New(layout.NewHBoxLayout(), rowlabel, rows, columnlabel, columns)

	// create Operation H box
	var operationtype string
	fmt.Println(operationtype)

	label := widget.NewLabel("Wählen Sie Ihre gewünschte Operation aus")
	options := []string{"Determinante", "ref", "rref"}
	choice := widget.NewSelect(options, func (val string){
		operationtype = val
	})
	operation := container.New(layout.NewHBoxLayout(), label, choice)

	// create tab container
	tab := container.New(layout.NewVBoxLayout(), crmatrix, matrix, operation)
	
	// create button for uimatrix
	entries := map[string]*widget.Entry{}
	var col, row int
	btn := widget.NewButton("Erstellen", func() {
		col = int(strconvnoerr(columns.Text))
		row = int(strconvnoerr(rows.Text))
		tab.Objects[1], entries = uimatrix(row, col)
		tab.Refresh()
	})
	crmatrix.Objects = append(crmatrix.Objects, btn)

	// create button for calc
	calcbutton := widget.NewButton("Ausführen", func(){
		arr := uimatrixtoarr(entries, row, col)
		mat := maths.NewMatrix(arr)
		switch operationtype {
		case "Determinante":
			determinant(myApp, *mat)
		case "ref":
			ref(myApp, *mat)
		case "rref": 
			rref(myApp, *mat)
		}
	})

	tab.Objects = append(tab.Objects, calcbutton)


	return tab
}