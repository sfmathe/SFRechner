package physics

import (
	"image"
	"image/color"
	"math"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"
)

type Zerfallsfunktion struct {
	N0          float64
	Koeffizient float64
	Halbwert    float64
}

func (obj *Zerfallsfunktion) Init() {
	if obj.Koeffizient == 0 {
		obj.Koeffizient = math.Ln2 / obj.Halbwert
	}
	if obj.Halbwert == 0 {
		obj.Halbwert = math.Ln2 / obj.Koeffizient
	}
}
																	//
func (z Zerfallsfunktion) Plot(myApp fyne.App) {
	p := plot.New()

	p.Title.Text = "Test Function"
	p.X.Label.Text = "x"
	p.Y.Label.Text = "y"

	var N0, lambda float64 = z.N0, z.Koeffizient
	function := plotter.NewFunction(func(x float64) float64 {
		return N0 * math.Pow(math.E, -1*lambda*x)
	})
	function.Color = color.RGBA{0, 0, 0, 255}

	p.Add(function)
	p.Legend.Add("Decay", function)

	p.Legend.ThumbnailWidth = 0.5 * vg.Inch

	p.X.Min = 0
	p.X.Max = 5 * z.Halbwert
	p.Y.Min = 0
	p.Y.Max = N0
																	//
	img := image.NewRGBA(image.Rect(0, 0, 1920, 1080))
	c := vgimg.NewWith(vgimg.UseImage(img))
	canv := draw.New(c)
	p.Draw(canv)

	w := myApp.NewWindow("Graph")
	fyneimg := canvas.NewImageFromImage(c.Image())
	w.SetContent(fyneimg)
	w.Resize(fyne.NewSize(1280, 720))
	w.Show()
}

func (z Zerfallsfunktion) Eval(x float64) float64 {
	return z.N0 * math.Pow(math.E, -1*z.Koeffizient*x)
}

func (z Zerfallsfunktion) Activity(N float64) float64 {
	return z.Koeffizient * N
}

func (z Zerfallsfunktion) Avglife() float64 {
	return 1 / z.Koeffizient
}
