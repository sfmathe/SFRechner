package physics

import (
	"math"
)

type Wave struct {
	Wavelength float64
	Period     float64
	Frequency  float64
	Velocity   float64
	Angfreq    float64 //angular Frequency
	Angwavenr  float64 // angular wavenumber
}

func (obj *Wave) Init() {
	if obj.Wavelength != 0 {
		obj.Frequency = obj.Velocity / obj.Wavelength
	}
	if obj.Period != 0 {
		obj.Frequency = 1 / obj.Period
	}
	if obj.Frequency != 0 {
		obj.Wavelength = obj.Velocity / obj.Frequency
		obj.Period = 1 / obj.Frequency
		obj.Angfreq = (2 * math.Pi) * obj.Frequency
	}
	if obj.Wavelength != 0 {
		obj.Angwavenr = (2 * math.Pi) / obj.Wavelength
	}

}

// "spalt/gitter" slit

type Slit struct {
	N           int64   // single or grid, 1 = single, 2 = grid
	Extremetype string  // either minima or maxima
	D           float64   // Gitterkonstante oder spaltbreite
	Dtodetector float64 // distance to detector
}

func (obj *Slit) Init() {
	obj.Extremetype = "minima"
	if obj.N== 2 {
		obj.Extremetype = "maxima"
	}
}

// calc angle of either maxima or minima in Radians
func (s Slit) Angle(wavelength float64, order int64) float64 {
	inner := float64(order) * (wavelength / s.D)
	return math.Asin(inner)
}

//calc distance between extremevalues on detector
func (s Slit) Distance(wavelength float64, order int64) float64 {
	angle := s.Angle(wavelength, order)
	return math.Sin(angle) * s.Dtodetector
}
