package physics

const ( // BeFoTa S. 268
	C     float64 = 2.998e8
	Emass float64 = 9.109e-31
	H     float64 = 6.626e-34
)

// create electron object

type Electron struct {
	mass      float64
	debroglie float64
	charge    float64
	velocity  float64
	momentum  float64 // "Impuls"
}

func (obj *Electron) Init() {
	obj.mass = Emass
	obj.charge = -1
	if obj.velocity != 0 {
		obj.momentum = obj.mass * obj.velocity
	}
	if obj.momentum != 0 && obj.velocity == 0 {
		obj.velocity = obj.momentum / obj.mass
	}
	if obj.momentum == 0 && obj.debroglie != 0 {
		obj.momentum = H / obj.debroglie
	}
	if obj.debroglie == 0 && obj.momentum != 0 {
		obj.debroglie = H / obj.momentum
	}
}

//create photon object

type Photon struct {
	velocity   float64
	energy     float64
	wavelength float64
	frequency  float64
}

func (obj *Photon) Init() {
	obj.velocity = C
	if obj.wavelength != 0 {
		obj.frequency = C / obj.wavelength
	}
	if obj.frequency != 0 {
		obj.energy = H * obj.frequency
		obj.wavelength = C / obj.frequency
	}
}

// refractive indices for optics in wavelength = 589 nm

var Refrindices = map[string]float64{
	"Luft":       1.0, // actually 1.000272
	"Wasser":     1.333,
	"Glas":       1.51814, // Jenaer Glass K3
	"Diamant":    2.4173,
	"Quarzglass": 1.4584,
}
