package physics

import (
	"math"
)

type Relativityobject struct {
	//classicvel    float64 // for questions where Velocity is searched
	Velocity      float64 // v
	Lorentz       float64 // Lorentzfaktor, calc in init if v is given
	Time0         float64 // t0
	Reltime       float64 // t'
	Restlength    float64 // l0
	Rellength     float64 // 1'
	Restmass      float64 // m
	Relmass       float64 // mrel
	Momentum      float64 // "impuls" p
	Restenergy    float64 // calc in init if given m
	Fullenergy    float64 // calc in init if given Velocity and m
	Kineticenergy float64 // calc in init if given Velocity and m is given
}

// constructor function to check which values are inputted and what can be calculated using those values
func (obj *Relativityobject) Init() { // all formulas are found on BeFoTa page 181
	//if statements check if theres an input for the fields, which then are used to calculate other entries
	if obj.Velocity != 0 {
		obj.Lorentz = 1 / math.Sqrt(1-(math.Pow((obj.Velocity/C), 2)))
	}
	if obj.Lorentz != 0 && obj.Time0 != 0 {
		obj.Reltime = obj.Lorentz * obj.Time0
	}
	if obj.Lorentz != 0 && obj.Restlength != 0 {
		obj.Rellength = obj.Restlength / obj.Lorentz
	}
	if obj.Lorentz != 0 && obj.Restmass != 0 {
		obj.Relmass = obj.Lorentz * obj.Restmass
	}
	if obj.Velocity != 0 && obj.Relmass != 0 {
		obj.Momentum = obj.Velocity * obj.Relmass
	}
	if obj.Restmass != 0 {
		obj.Restenergy = obj.Restmass * C
	}
	if obj.Restenergy != 0 { // there are two ways to calculate Fullenergy
		if obj.Lorentz != 0 {
			obj.Fullenergy = obj.Restenergy * obj.Lorentz
		}
		if obj.Kineticenergy != 0 {
			obj.Fullenergy = obj.Kineticenergy + obj.Restenergy
		}

	}
	if obj.Restenergy != 0 && obj.Lorentz != 0 {
		obj.Kineticenergy = (obj.Lorentz - 1) * obj.Restenergy
	}

}

/** von t und t0 auf v
	von l und l0 auf v
	impuls aus geschwindigkeit und iwas



**/
