package physics

import (
	"math"
)

type Beam struct {
	Initangle  float64 // "einfallswinkel"
	Anglestate string  // check if angle is radian or degree
	Medium     string  // in what medium is the beam
	Speed      float64 // defined as c in init
	Refrindex  float64 // "brechungsindex" n
}

func (obj *Beam) Init() {
	if obj.Medium == "" {
		obj.Medium = "Luft"
		obj.Speed = C
	}
	if _, found := Refrindices[obj.Medium]; found {
		obj.Refrindex = Refrindices[obj.Medium]
	}
}

// method to calc new angle using a second refractive Index
func (b Beam) Refraction(n2 float64) float64 {
	if b.Anglestate != "degree" {
		b.Anglestate = "radian"
		b.Initangle = (b.Initangle / 180) * math.Pi
	}
	inside := (b.Refrindex / n2) * math.Sin(b.Initangle)

	return math.Asin(inside)
}

type Lensesystem struct {
	Pf float64 // focal length "brennweite"
	PD float64 // "Brechkraft"
	Pg float64 // "Gegenstandsweite"
	Pb float64 // "Bildweite"
	PG float64 // "Gegenstandsgrösse"
	PB float64 // "Bildgrösse"
}

func (obj *Lensesystem) Init() {
	// check values entered and calculate everything possible
	if obj.Pb == 0 && obj.Pg!= 0 && obj.PB != 0 && obj.PG != 0 {
		obj.Pb = (obj.PB * obj.Pg) / obj.PG
	}
	if obj.Pg== 0 && obj.Pb != 0 && obj.PB != 0 && obj.PG != 0 {
		obj.Pg= (obj.Pb * obj.PG) / obj.PB
	}
	if obj.PB == 0 && obj.Pg!= 0 && obj.Pb != 0 && obj.PG != 0 {
		obj.PB = (obj.Pb * obj.PG) / obj.Pg
	}
	if obj.PG == 0 && obj.Pg!= 0 && obj.Pb != 0 && obj.PB != 0 {
		obj.PB = (obj.Pg* obj.PB) / obj.Pb
	}
	if obj.Pf== 0 && obj.Pb != 0 && obj.Pg!= 0 {
		obj.Pf= (obj.Pb * obj.Pg) / (obj.Pb + obj.Pg)
	}
	if obj.Pg == 0 && obj.Pf != 0 && obj.PB !=0 && obj.PG != 0 {
		obj.Pg = (obj.Pf * (obj.PB + obj.PG)) / obj.PB
	}
	if obj.Pb == 0 && obj.Pf != 0 && obj.PB != 0 && obj.PG != 0 {
		obj.Pb = (obj.Pf * (obj.PB + obj.PG)) / obj.PG
	}
	if obj.Pb != 0 && obj.Pf!= 0 {
		obj.Pg= (obj.Pb * obj.Pf) / (obj.Pb - obj.Pf)
	}
	if obj.Pb == 0 && obj.Pg!= 0 && obj.Pf!= 0 {
		obj.Pb = (obj.Pg* obj.Pf) / (obj.Pf- obj.Pg)
	}
	if obj.Pf!= 0 {
		obj.PD = 1 / obj.Pf
	}
}
