package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sfmathe/sfrechner/physics"
)

func distancefunc(myApp fyne.App, slit physics.Slit, order int64, wavelength float64 ) {
	if slit.Dtodetector == 0.0 {
		return
	}
	var distances []float64
	// define end and n so that if there is a value for which Order, it only does one loop
	var n, end int64
	n, end = 0, 6 
	if order != 0 {
		n = order
		end = n+1
	}
	for i := n; i < end; i++ {
		distances = append(distances, slit.Distance(wavelength, i))
	}

	var cont = container.New(layout.NewVBoxLayout())

	for i, j := range distances{
		n := int64(i)
		if len(distances) == 1{
			n = order
		}
		str := fmt.Sprintf("Die Distanz zwischen der 0. Extremstelle und der %d. beträgt %f", n, j)
		cont.AddObject(widget.NewLabel(str))
	}
	w := myApp.NewWindow("Distanz")
	w.SetContent(cont)
	w.Show()
}

func anglefunc(myApp fyne.App, slit physics.Slit, order int64, wavelength float64){
	var angles []float64
	// define end and n so that if there is a value for which Order, it only does one loop
	var n, end int64
	n, end = 0, 6 
	if order != 0 {
		n = order
		end = n+1
	}
	for i := n; i < end; i++ {
		angles = append(angles, slit.Angle(wavelength , i))
	}

	// Make labels for Canvasobject of new Window
	var cont = container.New(layout.NewVBoxLayout())

	for i, j := range angles{
		n := int64(i)
		if len(angles) == 1{
			n = order
		}
		str := fmt.Sprintf("Der Winkel der %d. Ordnung beträgt %f Radian", n, j)
		cont.AddObject(widget.NewLabel(str))
	}

	w := myApp.NewWindow("Winkel")
	w.SetContent(cont)
	w.Show()
}