package main

import (
	"fmt"
	"image/color"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sfmathe/sfrechner/physics"
)



func Genconsttab() fyne.CanvasObject {
	subtitle := fmt.Sprintf(
		`Es folgen wichtige Konstanten der Physik und wenige Angaben zu den häufigsten Brechungsindizes`,
	)
	fotabe := fmt.Sprintf(
		` Die Konstanten befinden sich im hinteren Bücherumschlag des Fotabe. Die Brechungsindizes auf Seite 193`,
	)
	header := Makeheader("Konstanten", subtitle, fotabe)

	// creating a container for the constants
	lightspeed := fmt.Sprintf("Die Lichtgeschwindigkeit in einem Vakuum beträgt %e m/s", physics.C)
	electronmass := fmt.Sprintf("Die Masse eines Elektrons beträgt %e kg", physics.Emass)
	plankc := fmt.Sprintf("Die Planck-Konstante beträgt %e J*s", physics.H)
	

	// creating a container for the refractive indices
	keys, values := Maptoarr(physics.Refrindices)
	refrindices := widget.NewList(
		func() int {
			return len(physics.Refrindices)
		},
		func() fyne.CanvasObject {
			return widget.NewLabel("template")
		},
		func(i widget.ListItemID, o fyne.CanvasObject) {
			str := fmt.Sprintf("%s : %f", keys[i], values[i])
			o.(*widget.Label).SetText(str)
		})

	// create tab
	tab := container.New(layout.NewVBoxLayout(), header,
	canvas.NewText(lightspeed, color.White), canvas.NewText(electronmass, color.White), 
	canvas.NewText(plankc, color.White), refrindices)

	return tab

}

func genoptictab(myApp fyne.App) fyne.CanvasObject{
	// header
	fotabe1 := "Die Formeln stammen aus dem Fotabe auf Seite 163"
	header1 := Makeheader("Optik", "", fotabe1)

	// Beam Tab
	var anglestate string
	initangle := widget.NewEntry()
	medium := widget.NewEntry()
	refrindex := widget.NewEntry()
	options := []string{"radian", "degree"}
	anglechoice := widget.NewSelect(options, func (value string) {
		anglestate = value
	})

	beamform := &widget.Form{
		Items: []*widget.FormItem{ },
		OnSubmit: func() { 
			floatangle, _ := strconv.ParseFloat(initangle.Text, 64)
			floatrefr, _ := strconv.ParseFloat(refrindex.Text, 64)
			beam := physics.Beam{
				Initangle : floatangle,
				Medium : medium.Text,
				Anglestate: anglestate,
			}
			beam.Init()
			newangle := beam.Refraction(floatrefr)
			println(beam.Refrindex)
			w := myApp.NewWindow("Strahl")
			strnewangle := fmt.Sprintf("Der Brechungswinkel beträgt %f", newangle)
			w.SetContent(widget.NewLabel(strnewangle))
			w.Show()
		},
	}
	
	beamform.Append("Einfallswinkel", initangle)
	beamform.Append("Medium", medium)
	beamform.Append("Brechungsindex 2. Medium", refrindex)
	beamform.SubmitText = "Go"
	beamtab := container.New(layout.NewVBoxLayout(), header1, anglechoice, beamform)

	//Lensesystems

	subtitle2 := "Hier kann man die Werte eingeben die man kennt, und das Programm rechnet alle möglichen Werte aus"
	fotabe2 := "Alle Formeln finden sich auf Seite 167"
	header2 := Makeheader("Linsen", subtitle2, fotabe2)

	entries := Makeentries("f", "D", "b", "B", "g", "G")

	lenseform := &widget.Form{
		Items: []*widget.FormItem{ },
		OnSubmit: func() {
			floats := Makefloats(entries)
			lensesystem := physics.Lensesystem{
				Pf : floats["f"],
				PD : floats["D"],
				Pb : floats["b"],
				PB : floats["B"],
				Pg : floats["g"],
				PG : floats["G"], 
			}
			lensesystem.Init()
			flabel := floatlabelf("Die Brennweite f beträgt %f Meter", lensesystem.Pf)
			Dlabel := floatlabelf("Die Brechkraft D beträgt %f Dioptrien", lensesystem.PD)
			blabel := floatlabelf("Die Bildweite b beträgt %f Meter", lensesystem.Pb)
			Blabel := floatlabelf("Die Bildgrösse B beträgt %f Meter", lensesystem.PB)
			glabel := floatlabelf("Die Gegenstandsweite g beträgt %f Meter", lensesystem.Pg)
			Glabel := floatlabelf("Die Gegenstandsgrösse G beträgt %f Meter", lensesystem.PG)
			cont := container.New(layout.NewVBoxLayout(), flabel, Dlabel, blabel, Blabel, glabel, Glabel)
			w := myApp.NewWindow("Linsensystem")
			w.SetContent(cont)
			w.Show()

		},
	}
	Appendform(lenseform, entries, opticparas)
	lensetab := container.New(layout.NewVBoxLayout(), header2, lenseform)

	tab := container.NewAppTabs(
		container.NewTabItem("Strahlen", beamtab),
		container.NewTabItem("Linsensysteme", lensetab),
	)

	return tab
}

func genwavetab(myApp fyne.App) fyne.CanvasObject {

	// Tab for wave calculations
	subtitle := "Auf diesem Tab kann man alle Angaben die man zu einer Welle hat eingeben, und das Programm rechnet die Restlichen aus."
	fotabe := "Die Formeln zu den Wellen lassen sich auf Seite 166 finden."
	header := Makeheader("Wellen", subtitle, fotabe)
	
	Lambda := widget.NewEntry()
	T := widget.NewEntry()
	f := widget.NewEntry()
	v := widget.NewEntry()
	w := widget.NewEntry()
	k := widget.NewEntry()
	waveform := &widget.Form{
		Items: []*widget.FormItem{ },
		OnSubmit: func() {
			wave := physics.Wave{
				Wavelength : strconvnoerr(Lambda.Text),
				Period : strconvnoerr(T.Text),
				Frequency : strconvnoerr(f.Text),
				Velocity : strconvnoerr(v.Text),
				Angfreq : strconvnoerr(w.Text),
				Angwavenr : strconvnoerr(k.Text), 
			}
			wave.Init()
			label1 := widget.NewLabel(fmt.Sprintf("Die Wellenlänge %s beträgt %f Meter", lambda, wave.Wavelength))
			label2 := floatlabelf("Die Periode T beträgt %f s", wave.Period)
			label3 := floatlabelf("Die Frequenz f beträgt %f hz", wave.Frequency)
			label4 := floatlabelf("Die Geschwindigkeit v beträgt %f m/s", wave.Velocity)
			label5 := floatlabelf("Die Kreisfrequenz w beträgt %f hz", wave.Angfreq)
			label6 := floatlabelf("Die Kreiswellenzahl k beträgt %f 1/m", wave.Angwavenr)
			cont := container.New(layout.NewVBoxLayout(), label1, label2, label3, label4, label5, label6)
			w := myApp.NewWindow("Welle")
			w.SetContent(cont)
			w.Show()

		},
	}
	waveform.Append(fmt.Sprintf("Die Wellenlänge %s in SI-Einheiten", lambda ), Lambda)
	waveform.Append("Die Periode T in SI-Einheiten", T)
	waveform.Append("Die Frequenz f in SI-Einheiten", f)
	waveform.Append("Die Geschwindigkeit v in SI-Einheiten", v)
	waveform.Append("Die Kreisfrequenz w in SI-Einheiten", w)
	waveform.Append("Die Kreiswellenzahl k in SI-Einheiten", k)

	wavetab := container.New(layout.NewVBoxLayout(), header, waveform )

	// Tab for Slit calculations

	subtitle = "Hier kann man Spaltberechnungen (Winkel und Distanz vom Schirm) machen."
	fotabe = "Die Formeln stehen im FoTaBe auf Seite 168"
	header = Makeheader("Spalt", subtitle, fotabe)

	d := widget.NewEntry()
	D := widget.NewEntry()
	Lambda = widget.NewEntry()
	n := widget.NewEntry()

	var slittype int
	options1 := []string{"Gitter", "Einzelspalt"}
	slitchoice := widget.NewSelect(options1, func (value string) {
		slittype = slitmap[value]
	})
	var calctype string
	options2 := []string{"Winkel", "Distanz"}
	calcchoice := widget.NewSelect(options2, func (value string){
		calctype = value
	})
	slitform := &widget.Form{
		Items: []*widget.FormItem{ },
		OnSubmit: func() {
			slit := physics.Slit{
				N : int64(slittype),
				D : strconvnoerr(D.Text),
				Dtodetector : strconvnoerr(d.Text),
			}
			slit.Init()
			order,_ := strconv.ParseInt(n.Text, 0,0)
			wavelength := strconvnoerr(Lambda.Text)
			switch calctype {
			case "Winkel":
				anglefunc(myApp, slit, order, wavelength)
			
			case "Distanz":
				distancefunc(myApp, slit, order, wavelength)
			}},
	}
	slitform.Append("Die Gitterkonstante bzw. der Spaltabstand d in nm", D)
	slitform.Append("Die Distanz zum Detektor D in SI-Einheiten", d)
	slitform.Append(fmt.Sprintf("Die Wellenlänge %s in nm", lambda), Lambda)
	slitform.Append("Die Ordnung der Extremstelle n", n)

	slittab := container.New(layout.NewVBoxLayout(), header, slitchoice, calcchoice, slitform)

	//make Apptabs
	tab := container.NewAppTabs(
		container.NewTabItem("Wellen", wavetab),
		container.NewTabItem("Spalt", slittab),
	)
	return tab
}

func genradioactivitytab(myApp fyne.App) fyne.CanvasObject {
	subtitle := 
	"Hier kann man durch das Auswahl-Menu die gewünschte Funktion auswählen und dann ermitteln"
	fotabe := "Die Formeln finden sich im FoTaBe auf Seite 183"
	header := Makeheader("Radioaktivität", subtitle, fotabe)

	var calctype, inputtype string
	
	initial := widget.NewEntry()
	initialitem := widget.NewFormItem("Startwert", initial)
	coeff := widget.NewEntry()
	coeffitem := widget.NewFormItem("Zerfallskonstante", coeff)
	halflife := widget.NewEntry()
	halflifeitem := widget.NewFormItem("Halbwertszeit", halflife)
	amount := widget.NewEntry()
	amountitem := widget.NewFormItem("Anzahl Kerne", amount)
	time := widget.NewEntry()
	timeitem := widget.NewFormItem("Wie viel Zeit in s", time)

	basicformarr := []*widget.FormItem{} // basic array for other items to be added on

	radioactivityform := &widget.Form{
		Items: basicformarr,
	}
	options := []string{"Zeichnen", "Ausrechnen", "Aktivität", "Mittlere Lebensdauer"}
	calcchoice := widget.NewSelect(options, func(value string){
		var coefforhalflife = halflifeitem

		if inputtype == "Zerfallskonstante"{
			coefforhalflife = coeffitem
		}
		calctype = value
		switch calctype{
		case "Zeichnen":
			radioactivityform.Items = append(basicformarr, initialitem, coefforhalflife)
		case "Ausrechnen":
			radioactivityform.Items = append(basicformarr, initialitem, coefforhalflife, timeitem)
		case "Aktivität":
			radioactivityform.Items = append(basicformarr, coefforhalflife, amountitem )
		case "Mittlere Lebensdauer":
			radioactivityform.Items = append(basicformarr, coefforhalflife)
		}
		radioactivityform.Refresh()
	})

	options = []string{"Zerfallskonstante", "Halbwertszeit"}
	inputchoice := widget.NewSelect(options, func (value string) {
		inputtype = value
		calcchoice.Refresh()
	})

	radioactivityform.OnSubmit =  func() {
		decay := physics.Zerfallsfunktion{
			N0 : strconvnoerr(initial.Text),
			Koeffizient: strconvnoerr(coeff.Text),
			Halbwert: strconvnoerr(halflife.Text),
		}
		decay.Init()
		switch calctype {
		case "Ausrechnen":
			eval(decay, myApp, strconvnoerr(time.Text))
		case "Zeichnen":
			decay.Plot(myApp)
		case "Aktivität":
			activity(decay, myApp, strconvnoerr(amount.Text))
		case "Mittlere Lebensdauer":
			midlife(decay, myApp)
	}}

	cont := container.New(layout.NewVBoxLayout(), header, inputchoice,
	 calcchoice, radioactivityform)
	return cont
}

func genrelativitytab(myApp fyne.App) fyne.CanvasObject {
	subtitle := "Auf diesem Tab kann man alle angegebenen Werte einschreiben, um (fast) alle möglichen Werte herauszubekommen"
	fotabe := "Die Formeln befinden sich auf Seite 181 im FoTaBe"
	header := Makeheader("Relativitätstheorie", subtitle, fotabe)

	entries := Makeentries("velocity", "time0", "reltime", "restlength", "rellength",
	"restmass", "relmass", "momentum", "restenergy", "fullenergy", "kineticenergy")
	
	relativityform :=  &widget.Form{
		Items: []*widget.FormItem{ },
		OnSubmit: func() {
			floats := Makefloats(entries)
			relobject := physics.Relativityobject{
				Velocity : floats["velocity"],
				Lorentz: 0,
				Time0: floats["time0"],
				Reltime: floats["reltime"],
				Restlength: floats["restlength"],
				Rellength: floats["relllength"],
				Restmass: floats["restmass"],
				Relmass: floats["relmass"],	
				Momentum : floats["momentum"],
				Restenergy: floats["restenergy"],
				Fullenergy: floats["fullenergy"],
				Kineticenergy: floats["kineticenergy"],
			}
			relobject.Init()
			relobject.Init()
			label1 := widget.NewLabel(fmt.Sprintf("Die relativistische Geschwindigkeit beträgt %e m/s", relobject.Velocity))
			label2 := widget.NewLabel(fmt.Sprintf("Der Lorentzfaktor beträgt %e", relobject.Lorentz))
			label3 := widget.NewLabel(fmt.Sprintf("Die Zeit eines externen Beobachters beträgt %f s", relobject.Time0))
			label4 := widget.NewLabel(fmt.Sprintf("Die Zeit in einem bewegtem System beträgt %f s", relobject.Reltime))
			label5 := widget.NewLabel(fmt.Sprintf("Die Länge in einem ruhendem System beträgt %e m", relobject.Restlength))
			label6 := widget.NewLabel(fmt.Sprintf("Die Länge in einem bewegtem System beträgt %e m", relobject.Rellength))
			label7 := widget.NewLabel(fmt.Sprintf("Die Ruhemasse beträgt %e kg", relobject.Restmass))
			label8 := widget.NewLabel(fmt.Sprintf("Die Masse in einem bewegtem System beträgt %e kg", relobject.Relmass))
			label9 := widget.NewLabel(fmt.Sprintf("Der relativistische Impuls beträgt %e m*kg/s", relobject.Momentum))
			label10 := widget.NewLabel(fmt.Sprintf("Die Ruheenergie beträgt %e J", relobject.Restenergy))
			label11 := widget.NewLabel(fmt.Sprintf("Die Gesamtenergie beträgt %e J", relobject.Fullenergy))
			label12 := widget.NewLabel(fmt.Sprintf("Die relativistische kinetische Energie beträgt %e J", relobject.Kineticenergy))

			cont := container.New(layout.NewVBoxLayout(), label1, label2, label3, label4, label5, label6, label7,
			label8, label9, label10, label11, label12)

			w := myApp.NewWindow("Relativität")
			w.SetContent(cont)
			w.Show()
		}}
		relativityform.Append("Die Geschwindigkeit in m/s", entries["velocity"])		
		relativityform.Append("Die Zeit in einem ruhendem System in s", entries["time0"])
		relativityform.Append("Die Zeit in einem bewegtem System in s", entries["reltime"])
		relativityform.Append("Die Länge in einem ruhendem System in m", entries["restlength"])
		relativityform.Append("Die Länge in einem bewegtem System in m", entries["rellength"])
		relativityform.Append("Die Ruhemasse in kg", entries["restmass"])
		relativityform.Append("Die Masse in einem bewegtem System in kg", entries["relmass"])
		relativityform.Append("Der relativistische Impuls in SI-Einheiten", entries["momentum"])
		relativityform.Append("Die Ruheenergie in J", entries["restenergy"])
		relativityform.Append("Die Gesamtenergie in J", entries["fullenergy"])
		relativityform.Append("Die kinetische Energie in J", entries["kineticenergy"])

		cont := container.New(layout.NewVBoxLayout(), header, relativityform)
		contscroll := container.NewVScroll(cont)
	return contscroll
}