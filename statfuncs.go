package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/sfmathe/sfrechner/maths"
)

func prediction(myApp fyne.App, b maths.Binomial, security int16) {
	intervall, err := b.Prediction(security)

	str := fmt.Sprintf("Das Prognoseintervall beträgt %v", intervall)

	if  err != nil{
		str = fmt.Sprintf("Fehler: %v", err)
	}
	
	showwindow(widget.NewLabel(str), myApp, "Prognoseintervall")

}

func confidence(myApp fyne.App, sample maths.Sample, security int16) {

	intervall, err := sample.Confidence(security)

	str := fmt.Sprintf("Das Intervall beträgt %v", intervall)

	if err != nil{
		str = fmt.Sprintf("Fehler: %v", err)
	}

	showwindow(widget.NewLabel(str), myApp, "Konfidenzintervall")

}

func errtolerance(myApp fyne.App, sample maths.Sample, security int16) {
	val, err := sample.Tolerance(security)

	str := fmt.Sprintf("Die Fehlertoleranz beträgt %f", val)

	if err != nil{
		str = fmt.Sprintf("Fehler: %v", err)
	}

	showwindow(widget.NewLabel(str), myApp, "Fehlertoleranz")
}

func Samplesize(myApp fyne.App, guess float64, tolerance float64, guesstype string, security int16)  {
	var val float64
	var err error
	switch guesstype{
	case "Mit Vermutung":
		val, err = maths.Samplesizewithguess(guess, tolerance,security) 
	case "Ohne Vermutung":
		val, err = maths.Samplesize(tolerance, security)
	}

	str := fmt.Sprintf("Der Stichprobenumfang beträgt %f", val)

	if err != nil{
		str = fmt.Sprintf("Fehler: %v", err)
	}
	showwindow(widget.NewLabel(str), myApp, "Stichprobenumfang")

}