module gitlab.com/sfmathe/sfrechner

go 1.18

require (
	fyne.io/fyne/v2 v2.1.4
	gitlab.com/sfmathe/sfrechner/maths v0.0.0-00010101000000-000000000000
	gitlab.com/sfmathe/sfrechner/physics v0.0.0-00010101000000-000000000000
)

require (
	git.sr.ht/~sbinet/gg v0.3.1 // indirect
	github.com/ajstarks/svgo v0.0.0-20211024235047-1546f124cd8b // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fredbi/uri v0.0.0-20181227131451-3dcfdacbaaf3 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-fonts/liberation v0.2.0 // indirect
	github.com/go-gl/gl v0.0.0-20210813123233-e4099ee2221f // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20211024062804-40e447a793be // indirect
	github.com/go-latex/latex v0.0.0-20210823091927-c0d11ff05a81 // indirect
	github.com/go-pdf/fpdf v0.6.0 // indirect
	github.com/godbus/dbus/v5 v5.0.4 // indirect
	github.com/goki/freetype v0.0.0-20181231101311-fa8a33aabaff // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/srwiley/oksvg v0.0.0-20200311192757-870daf9aa564 // indirect
	github.com/srwiley/rasterx v0.0.0-20200120212402-85cb7272f5e9 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/yuin/goldmark v1.4.1 // indirect
	golang.org/x/exp v0.0.0-20220428152302-39d4317da171 // indirect
	golang.org/x/image v0.0.0-20220413100746-70e8d0d3baa9 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
	golang.org/x/text v0.3.7 // indirect
	gonum.org/v1/plot v0.11.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

replace gitlab.com/sfmathe/sfrechner/maths => ./maths

replace gitlab.com/sfmathe/sfrechner/physics => ./physics
